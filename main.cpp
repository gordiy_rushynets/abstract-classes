//
//  main.cpp
//  AbstractClasses
//
//  Created by Gordiy Rushynets on 3/5/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;

/*
 Клас в якому присутня чиста віртуальна функція (тобто ця функція не містить реалізації)
 називається АБСТРАКТНИМ!!! Екземпляри (об’єкти) абстрактного класу компілятор не дозволяє створювати.
 
 Клас стає абстрактним якщо в ньому є хоча б одна чисто-віртувальна функція.
 */

class Weapon
{
public:
    // створили чисто-віртуальну функцію, тому клас Weapon є АБСТРАКТНИМ.
    virtual void shoot() = 0;
    
    // Якщо в Абстрактному класі є не тільки чисто-віртуальні функції то використовувати їх можна буде тільки
    // в наслідуваних класах
};

class Gun : public Weapon
{
public:
    virtual void shoot()
    {
        cout << "BANG!!!" << endl;
    }
};

class SubmachineGun : public Gun
{
public:
    void shoot() override
    {
        cout << "BANG BANG BANG !!!" << endl;
    }
};

class Bazooka : public Weapon
{
public:
    void shoot() override
    {
        cout << "BUM !!!" << endl;
    }
};

class Player
{
public:
    void shoot(Weapon *weapon)
    {
        weapon->shoot();
    }
};

class Knife : public Weapon
{
    void shoot() override
    {
        cout << "VZUH !!!" << endl;
    }
};


class A
{
public:
    A()
    {
        cout << "Виділена динамічна пам’ять. Об’єкт класу А" << endl;
    }
    
    ~A()
    {
        cout << "Вивільнена динамічна пам’ять. Об’єкт класу А" << endl;
    }
};

class B : public A
{
public:
    B()
    {
        cout << "Виділена динамічна пам’ять. Об’єкт класу B" << endl;
    }
    
    ~B()
    {
        cout << "Вивільнена динамічна пам’ять. Об’єкт класу B" << endl;
    }
};

int main(int argc, const char * argv[]) {
    // Екземпляри класів зброї
    Gun gun;
    SubmachineGun sub_gun;
    Bazooka bazooka;
    Knife knife;
    
    
    Player player;
    player.shoot(&gun);
    player.shoot(&sub_gun);
    player.shoot(&bazooka);
    player.shoot(&knife);
    
    // Віртуальний деструктор
    
    B *bptr =  new B;
    delete bptr;
    
    // ми не використовуємо ключового слова virtual тому, буде втрата пам’яті
    
    A *bpt =  new B;
    delete bpt;
    
    return 0;
}
